<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
echo "PHP Indexed Arrays </br>";
$cars = array("Volvo", "BMW", "Toyota");
echo "I like " . $cars[0] . ", " . $cars[1] . " and " . $cars[2] . ".";
echo "</br>";
var_dump ($cars);
echo "</br>";
print_r($cars);
echo "</br>";
echo "</br>";
echo "</br>";


echo "</br>PHP Associative Arrays </br>";
$age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");
echo "Peter is " . $age['Peter'] . " years old.";
echo "</br>";
var_dump ($age);
echo "</br>";
print_r($age);

echo "</br>";
echo "</br>";
echo "</br>";

$ar = array(
            "a",
            "b",
 15 => "d" ,
  10 => "e",
              "f",
    6 => "t",
                "m"
);
echo "</br>";
echo "</pre>";
print_r($ar);
echo "</pre>";